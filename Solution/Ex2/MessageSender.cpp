#include "MessageSender.h"

#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
#include "ColorCodes.h"

mutex _mutex;

MessageSender::MessageSender()
{
	_users = *new vector<string>();
	_messages = *new queue<string>();

	thread data(&MessageSender::handleDataFile, this);
	thread output(&MessageSender::sendQueue, this);

	startMain();

	data.detach();
	output.detach();
}

void MessageSender::handleDataFile()
{
	while (true)
	{
		ifstream file;
		file.open("data.txt");

		_mutex.lock();
		string line;
		while (getline(file, line))
		{
			_messages.push(string(line));
		}
		_mutex.unlock();	

		file.clear();
		file.close();

		ofstream clear;
		clear.open("data.txt");
		clear.close();

		this_thread::sleep_for(chrono::seconds(60));
	}
}

void MessageSender::sendQueue()
{
	while (true)
	{
		_mutex.lock();
		fstream write;
		write.open("output.txt");
		while (_messages.size())
		{
			for (string user : _users)
			{
				write << user << ": " << _messages.front() << "\n";
			}
			_messages.pop();
		}
		write.close();
		_mutex.unlock();
		this_thread::sleep_for(chrono::seconds(1));
	}
}

int MessageSender::getInput()
{
	int input;
	cin >> input;
	return input;
}

int MessageSender::getInput(int min, int max)
{
	int input;
	do
	{
		input = getInput();
	} while (input < min || input > max);
	return input;
}

void MessageSender::startMain()
{
	system("cls");

	cout << RESET_CC << YELLOW_CC << "Welcome to Magshimessage Sender!" << endl << endl;
	cout << RESET_CC << WHITE_CC << UNDERLINE_CC << BOLD_CC << "1." << RESET_CC << WHITE_CC << DARK_CC << " Sign In" << endl;
	cout << RESET_CC << WHITE_CC << UNDERLINE_CC << BOLD_CC << "2." << RESET_CC << WHITE_CC << DARK_CC << " Sign Out" << endl;
	cout << RESET_CC << WHITE_CC << UNDERLINE_CC << BOLD_CC << "3." << RESET_CC << WHITE_CC << DARK_CC << " Connected Users" << endl;
	cout << RESET_CC << WHITE_CC << UNDERLINE_CC << BOLD_CC << "4." << RESET_CC << WHITE_CC << DARK_CC << " Exit" << endl << endl;
	
	cout << RESET_CC << DARK_CC << "Enter Choice: " << BOLD_CC;

	switch (getInput(1, 4))
	{
	case 1:
		signIn();
		break;
	case 2:
		signOut();
		break;
	case 3:
		connectedUsers();
		break;
	case 4:
		system("cls");
		cout << RESET_CC << GREEN_CC << BOLD_CC << "Goodbye!" << endl << endl << BLACK_CC;
		system("pause");
		return;
	}

	startMain();
}

void MessageSender::signIn()
{
	system("cls");

	cout << RESET_CC << DARK_CC "Enter Your Name: " << BOLD_CC;

	string name;
	cin >> name;

	system("cls");

	if (isSignedIn(name))
	{
		cout << RESET_CC << BOLD_CC << RED_CC << "This user is already signed in." << BLACK_CC << endl << endl;
	}
	else
	{
		_users.push_back(name);
		cout << RESET_CC << BOLD_CC << GREEN_CC << "Successfully signed in." << BLACK_CC << endl << endl;
	}

	system("pause");
}

void MessageSender::signOut()
{
	system("cls");

	cout << RESET_CC << DARK_CC "Enter Your Name: " << BOLD_CC;

	string name;
	cin >> name;

	system("cls");

	if (removeUser(name))
	{
		cout << RESET_CC << BOLD_CC << GREEN_CC << "Successfully signed out." << BLACK_CC << endl << endl;
	}
	else
	{
		cout << RESET_CC << BOLD_CC << RED_CC << "This user is not signed in." << BLACK_CC << endl << endl;
	}

	system("pause");
}

void MessageSender::connectedUsers()
{
	system("cls");

	cout << RESET_CC << YELLOW_CC << UNDERLINE_CC << "Connected Users:" << endl << endl;

	if (!_users.size())
	{
		cout << RESET_CC << BOLD_CC << RED_CC "There aren't any logged-in users at the moment.";
	}

	for (int i = 0; i < _users.size(); i++)
	{
		cout << RESET_CC << WHITE_CC << UNDERLINE_CC << BOLD_CC << (i + 1) << "." << RESET_CC << WHITE_CC << DARK_CC << " " << _users[i] << endl;
	}

	cout << RESET_CC << BLACK_CC << BOLD_CC << endl << endl;

	system("pause");
}

bool MessageSender::isSignedIn(string user)
{
	for (string u : _users)
	{
		if (u == user)
		{
			return true;
		}
	}
	return false;
}

bool MessageSender::removeUser(string user)
{
	for (int i = 0; i < _users.size(); i++)
	{
		if (_users[i] == user)
		{
			_users.erase(_users.begin() + i);
			return true;
		}
	}
	return false;
}
