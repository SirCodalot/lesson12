#pragma once

#include <string>
#include <vector>
#include <queue>
#include <mutex>

using namespace std;

class MessageSender
{
private:
	vector<string> _users;
	queue<string> _messages;
	

	bool isSignedIn(string user);
	bool removeUser(string user);

	int getInput();
	int getInput(int min, int max);

	void startMain();

	void handleDataFile();
	void sendQueue();

public:
	MessageSender();

	void signIn();
	void signOut();
	void connectedUsers();
};